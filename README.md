Business Name: Law Offices of Alice Tavoukjian, PC

Address: 1055 E. Colorado Blvd. 5th Floor, Pasadena, CA 91106 USA

Phone: (626) 213-0683

Website: https://www.tavoukjianlaw.com

Description: The Law Offices of Alice Tavoukjian specializes in criminal and juvenile defense. For the last twelve years, the courtroom has been Alice's office. She has defended thousands of individuals, charged with both misdemeanors and felony offenses. Her years of experience has led Alice to successfully litigate both hundreds of pre-trial motions and dozens of jury trials.As a former attorney with the LA County Public Defender's Office-the oldest and largest public defender's office in the country-Alice has handled thousands of cases and contested hearings.Some of the charges she has handled or tried include: DUI, domestic violence, vehicular manslaughter, robbery, drug offenses, rape, sex crimes, child abuse, and homicide.

Keywords: criminal defense attorney At Pasadena, CA . attorney At Pasadena, CA. Pasadena, CA Law. PasadenaLegal.

Hour: Mon - Fri: 8 AM - 6 PM, Sat - Sun: By Appointment.

Payment: cash, credit/debit cards.

Year: 2019
